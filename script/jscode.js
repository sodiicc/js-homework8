let price = document.getElementById('price');

function validationOnBlur(num) {
  if (isNaN(num.value) || num.value < 0 || num.value[0] == 0 || num.value == '') {
    num.style.border = 'solid red 3px';
    error.innerText = 'Enter correct price pls !!!';
    return false;
  } else {
    num.value = num.value
    return true
  }
}
function validationOnFocus(num) {
  if (isNaN(num.value) || num.value < 0) {
    num.placeholder = 0;
    num.style.border = 'solid rgb(73, 238, 73) 3px';
    error.innerText = '';
  } else {
    num.style.border = 'solid rgb(73, 238, 73) 3px';
  }
}

price.onblur = function () {
  if (validationOnBlur(price)) {
    textPrice.textContent = 'Поточна ціна: $' + price.value;
    error.innerText = ''
    price.style.color = 'green'
    price.style.border = '';
    close.style.display = '';
    choosePrice.style.display = '';
    textPrice.style.display = '';
  };
};

price.onfocus = function () {
  validationOnFocus(price);
  price.value = ''
  price.style.color = ''
};

let close = document.getElementById('close');
let choosePrice = document.getElementById('choosePrice');
let textPrice = document.getElementById('priceText');
let error = document.getElementById('error');
close.style.display = 'none';
choosePrice.style.display = 'none';

close.onclick = () => {
  close.style.display = 'none';
  close.previousSibling.style.display = 'none';
  close.parentElement.style.display = 'none';
};